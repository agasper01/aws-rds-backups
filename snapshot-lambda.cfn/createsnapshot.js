/**
 * A lambda to create snapshots and put them in a S3 bucket
 */

var aws = require('aws-sdk');

exports.handler = function(event, context) {

    const debugging = {
        event, 
        context,
        'environmentVariables': process.env
    };
    console.log("****DEBUGGING****\n");
    console.log(debugging);
    const rds = new aws.RDS({
        region: process.env.REGION
    });
    // pretend it is easy to pick off the 
    // BOOOOOO => my memory of async await is HAZY 

    rds.describeInstances(async (err, data) => {
        if (err) {
            console.log('error describing instances');
            console.log(err, err.stack);
            return;
        } else {
            // iterate over the instances and use a filter
            // this is probably meant for a reduce
            // { "DBInstances": [ {}, {}, {} ] }
            console.log('describeInstances did not explode.');
            console.log('data', data);
            let masterDbInstances = data.DBInstances.map(async function(rdsInstance) {
                // force synchronous behavior
                let tags = await rds.listTagsForResource({ ResourceName: rdsInstance.DBInstanceArn }).promise(); 
                console.log('tags', tags);
                // pick off the TagList - Array<map>
                const { TagList } = tags;
                // if the instance has an IsMaster tag then return it
                // it is easier to tag master instances than it is for me to think of something clever
                let isMasterTags = TagList.filter( Tag => Tag.Key === "IsMaster" );
                // if the IsMaster tag is 
                if (isMasterTags.length) {
                    return rdsInstance;
                } else {
                    return;
                }
            });
            // for the master db instances createDbSnapshot
            masterDbInstances.map(function(dbInstance) {
                const timestamp = createTimestamp();
                let snapshotName = `${dbInstance.DBInstanceIdentifier}-lambda-snapshot-${timestamp}`;
                const snapshotParams = {
                    "DBSnapshotIdentifier": snapshotName,
                    "DBInstanceIdentifier": ""
                };
                const snapshotResponse = rds.createDbSnapshot(snapshotParams);
                console.log('snapshotResponse:', snapshotResponse);
            });
        }
        context.done(); // placeholder
    });

    

}

// lifted from some old pasta I wrote once upon a time - reduce, reuse, recycle
function createTimestamp() {
    let date = new Date();
    // Common Log Format: host ident authuser date request status bytes
    // YYYY-MM-DD:HH:mm:ss +/- nnnn
    // nnnn = time zone offet
    let YYYY = date.getFullYear();
    // JavaScript's date library has the months of the year running from 0-11, e.g. January = 0
    // That feel when the memes are too real
    let MM = date.getMonth()+1; 
    let DD = date.getDate(); 
    let HH = date.getHours(); 
    let mm = date.getMinutes();
    let ss = date.getSeconds();
    
    // Convert the numeric type to a string, and prefix any time values less than 10 with a leading 0 to maintain two sig. fig. format
    MM = (MM < 10) ? `0${MM}` : `${MM}`; 
    DD = (DD < 10) ? `0${DD}` : `${DD}`; 
    HH = (HH < 10) ? `0${HH}` : `${HH}`; 
    mm = (mm < 10) ? `0${mm}` : `${mm}`; 
    ss = (ss < 10) ? `0${ss}` : `${ss}`;
    // There's a switch/case statement for this situation, but I'm tired.
    // Is the number value an integer and is it positive?
    // wishful thinking has no place in production code => simple timestamp it is
    const commonLogFormatTimestampString  = `${YYYY}-${MM}-${DD}:${HH}:${mm}:${ss}`;
    return commonLogFormatTimestampString;
}
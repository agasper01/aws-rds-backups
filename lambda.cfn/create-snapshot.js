/**
 * A lambda to create snapshots and put them in a S3 bucket
 */

var aws = require('aws-sdk');

exports.handler = function(event, context) {

    const debugging = {
        event, 
        context,
        'environmentVariables': process.env
    };
    console.log("****DEBUGGING****\n");
    console.log(debugging);
    const rds = new aws.RDS({
        region: process.env.REGION
    });
    // pretend it is easy to pick off the 
    // BOOOOOO => my memory of async await is HAZY 

    rds.describeInstances(async (err, data) => {
        if (err) {
            console.log('error describing instances');
            console.log(err, err.stack)
            return;
        } else {
            // iterate over the instances and use a filter
            // this is probably meant for a reduce
            // { "DBInstances": [ {}, {}, {} ] }
            console.log('describeInstances did not explode.');
            console.log('data', data);
            masterDbInstances = data.DBInstances.map(function(rdsInstance) {
                // force synchronous behavior
                let tags = await rds.listTagsForResource({ ResourceName: rdsInstance.DBInstanceArn }).promise(); 
                console.log('tags', tags);
                // pick off the TagList - Array<map>
                const { TagList } = tags;
                // if the instance has an IsMaster tag then return it
                // it is easier to tag master instances than it is for me to think of something clever
                isMasterTags = TagList.filter( Tag => Tag.Key === "IsMaster" );
                // if the IsMaster tag is 
                if (isMasterTags.length) {
                    return rdsInstance
                } else {
                    return
                }
            });
            // for the master db instances createDbSnapshot
            masterDbInstances.map(function(dbInstance) {
                const timestamp = createTimestamp();
                snapshotName = `${dbInstance.DBInstanceIdentifier}-lambda-snapshot-${timestamp}`;
                const snapshotParams = {
                    "DBSnapshotIdentifier": snapshotName,
                    "DBInstanceIdentifier": ""
                };
                const snapshotResponse = rds.createDbSnapshot(snapshotParams);
                console.log('snapshotResponse:', snapshotResponse);
            });
        }
        context.done(); // placeholder
    });

    

}

// lifted from some old pasta I wrote once upon a time - reduce, reuse, recycle
function createTimestamp() {
    // Common Log Format: host ident authuser date request status bytes
    // YYYY-MM-DD:HH:mm:ss +/- nnnn
    // nnnn = time zone offet
    let YYYY = date.getFullYear();
    // JavaScript's date library has the months of the year running from 0-11, e.g. January = 0
    // That feel when the memes are too real
    let MM = date.getMonth()+1; 
    let DD = date.getDate(); 
    let HH = date.getHours(); 
    let mm = date.getMinutes();
    let ss = date.getSeconds();
    
    // console.log(`date.toTimeString(): ${date.toTimeString()}`);
    // dateObj.getTimeZoneOffset method returns a number representing the time-zone offset from UTC, in minutes, for the fdate based on current host system settings
    let timeZoneOffset = {
        'inHours': parseInt(date.toTimeString().slice(13,17))/100
    }; 
    timeZoneOffset.isInteger = timeZoneOffset.inHours % 1 === 0; 
    timeZoneOffset.isPositive = timeZoneOffset.inHours >= 0; // > || = to allow for the possibility that its UTC 0000
    // Convert the numeric type to a string, and prefix any time values less than 10 with a leading 0 to maintain two sig. fig. format
    MM = (MM < 10) ? `0${MM}` : `${MM}`; 
    DD = (DD < 10) ? `0${DD}` : `${DD}`; 
    HH = (HH < 10) ? `0${HH}` : `${HH}`; 
    mm = (mm < 10) ? `0${mm}` : `${mm}`; 
    ss = (ss < 10) ? `0${ss}` : `${ss}`;
    // There's a switch/case statement for this situation, but I'm tired.
    // Is the number value an integer and is it positive?
    if (timeZoneOffset.isInteger) {
        // If the timeZoneOffset.inHours is less than 10 but greater than 0 
        if (timeZoneOffset.isPositive) {
            timeZoneOffset.inHoursString = (timeZoneOffset.inHours < 10) ?  `+0${timeZoneOffset.inHours}00` : `+${timeZoneOffset.inHours}00`;  
        } else {
           // the number is negative but still an integer; Better phrasing? - The number is an integer and negative   
           // A two digit negative hour value greater than 10 will already have the -, so there is no need to add it.
           timeZoneOffset.inHoursString = (timeZoneOffset.inHours < 10) ?  `-0${timeZoneOffset.inHours}00` : `${timeZoneOffset.inHours}00`; 
        }
    } else {
        // the hours value is not an integer 
        if (timeZoneOffset.isPositive) {
            // Can still be positive though, e.g. 3.5
            // the isPositive seems a little silly now considering x < 0 && x < 10 would work 
           if  (timeZoneOffset.inHours < 10) {
               let timeZoneString = String(timeZoneOffset.inHour);
               timeSetOffset.inHoursString = (timeZoneString[2] === '5') ? `+0${timeZoneString[0]}30` : `+0${timeZoneString[0]}45`;    
           } else {
               // Still in positive float land, but for 2-digit values, 10-14, Still need to address the x:30 and x:45 cases 
            let timeZoneString = String(timeZoneOffset.inHourz);
            timeSetOffset.inHoursString = (timeZoneString[2] === '5') ? `+${timeZoneString[0]}30` : `+${timeZoneString[0]}45`;    
           }
        }
    }
    const commonLogFormatTimestampString  = `[${YYYY}-${MM}-${DD}:${HH}:${mm}:${ss}]`;
    return commonLogFormatTimestampString;
}